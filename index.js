function horaSegundos(){
    var variable=document.getElementById("resultadoHora");
    var minutos=60;
    var horas=3600;
    var horaActual=new Date();
    var hora= horaActual.getHours();
    var minuto=horaActual.getMinutes();
    var segundo=horaActual.getSeconds();
    var segundosHora=hora*horas;
    var segundosMinuto=minuto*minutos;
    var conversion=parseInt(segundosMinuto)+parseInt(segundosHora)
    +parseInt(segundo); 
    variable.innerHTML="Hora actual: "+hora+ ":"+minuto+ ":"+segundo+ " Resultado en segundos: " +conversion;
}
function obtenerArea(){
    var CalculosResultados=document.getElementById("resultadoArea");
    var b=parseInt(document.getElementById("base").value);
    var h=parseInt(document.getElementById("altura").value);
    var area=(b*h)/2; CalculosResultados.innerHTML="El área del triángulo es: "+area;
}
function raizCuadrada(){
    var raiz=document.getElementById("resultadoRaiz")
    var mensaje="El valor debe ser un número impar";
    var numero=document.getElementById("raiz").value;
    if(numero%2){
        var resultado=Math.sqrt(numero);
        raiz.innerHTML=""+resultado.toFixed(2);
    }
    else{
        raiz.innerHTML="" +mensaje;
    }
}
function cadenaTexto(){
    var longitudTexto=document.getElementById("resultadoCadena");
    var texto=document.getElementById("texto").value;
    var cadena= new String(texto);
    longitudTexto.innerHTML="La longitud de la cadena de texto es: " +cadena.length;
}
function concatenacion(){
    var resultadoConcatenar=document.getElementById("resultadoConcatenar");
    var array1=new Array("Lunes", " "+ "Martes", " "+ "Miercoles", " "+ "Jueves", " "+"Viernes");
    var array2=new Array(" "+"Sábado", " "+"Domingo");
    var diasTotales=array1.concat(array2);
    resultadoConcatenar.innerHTML="Días de la semana: "  + diasTotales;
}
function versionNavegador(){
    document.write("Versión del navegador: "+navigator.appVersion);
}
function tamañoPantalla(){
    var tamaño=document.getElementById("resultadoTamaño");
    tamaño.innerHTML="El ancho de la pantalla es: " +screen.width
    +" "+"La altura de la pantalla es: " +screen.height;
}
function imprimir(){
    if(window.print){
        window.print();
    }
}
